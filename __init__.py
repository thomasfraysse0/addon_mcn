bl_info = {
    "name": "Master Création Numérique 2021-2022",
    "author": "Various Artists",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "description": "Various tools (TBD)",
    "warning": "",
    "doc_url": "",
    "category": "Misc.",
}


import bpy
from . import (AL, AP, AY, CB, CC, CS, DH, EV, FA, HT, JM, LD, MB, MC, TF, VB)


class MCN_PT_Main_Panel(bpy.types.Panel):
    """"""
    bl_label = "Master Création Numérique"
    bl_idname = "MCN_PT_Main_Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Various tools")


def register():
    bpy.utils.register_class(MCN_PT_Main_Panel)
    for module in (AL, AP, AY, CB, CC, CS, DH, EV, FA, HT, JM, LD, MB, MC, TF, VB):
        module.register()


def unregister():
    for module in (AL, AP, AY, CB, CC, CS, DH, EV, FA, HT, JM, LD, MB, MC, TF, VB):
        module.unregister()
    bpy.utils.unregister_class(MCN_PT_Main_Panel)


if __name__ == "__main__":
    register()
