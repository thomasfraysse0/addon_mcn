import bpy

class MCN_OT_TF(bpy.types.Operator):
    """Add text object to scene"""
    bl_idname = "object.mcn_add_text"
    bl_label = "Add Text!"


    def execute(self, context):
        text = bpy.data.curves.new("My Text", "FONT")
        obj = bpy.data.objects.new("My Text", text)
        
        text_content = context.scene.mcn_text_content
        text.body = text_content
        
        bpy.context.collection.objects.link(obj)
        return {'FINISHED'}


class MCN_PT_TF(bpy.types.Panel):
    """"""
    bl_label = "TF"
    bl_idname = "MCN_PT_TF"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="Super opérateur")
        layout.prop(context.scene, "mcn_text_content" )
        layout.operator("object.mcn_add_text")


def register():
    bpy.types.Scene.mcn_text_content = bpy.props.StringProperty(name="Text content")

    bpy.utils.register_class(MCN_OT_TF)
    bpy.utils.register_class(MCN_PT_TF)


def unregister():
    bpy.utils.unregister_class(MCN_OT_TF)
    bpy.utils.unregister_class(MCN_PT_TF)

    del bpy.types.Scene.mcn_text_content